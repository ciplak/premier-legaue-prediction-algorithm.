let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');


// TODO need implement minify and merge css.

mix.styles([
  'path.min.css',
], 'public/dist/css/css_merged.css');

mix.scripts([
  'path.min.js',
], 'public/dist/js/js_merged.js');
