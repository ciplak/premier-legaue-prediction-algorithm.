<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "FrontController@getLeague")->name('getLeague');
Route::get('/play-all', "FrontController@play")->name('play');
Route::get('/play-week/{week}', "FrontController@playWeek")->name('playWeek');
Route::get('/edit-strenght', "FrontController@editStrenght")->name('editStrenght');


Route::get('generateFixture', "FrontController@generateFixture")->name('generateFixture');

Route::get('simulation', "FrontController@simulation")->name('simulation');

Route::group(['prefix' => 'api'], function(){

    Route::get('fixture', "FrontController@refreshFixture")->name('refreshFixture');


    Route::get('leauge', "FrontController@refreshLeauge")->name('refreshLeauge');
    Route::get('reset', "FrontController@reset")->name('reset');

    Route::get('next-matches/{week}', "FrontController@nextMatches")->name('nextMatches');
    Route::get('/update-match/{id}/{column}/{value}', "FrontController@updateMatch")->name('updateMatch');
    Route::get('/play-weekly/{week}', "FrontController@playWeekly")->name('playWeekly');

    Route::get('/predictions', "FrontController@predictions")->name('predictions');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
