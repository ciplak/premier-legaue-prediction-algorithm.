<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TeamTest extends TestCase
{
    private $noError;

    /**
     * Set Up.
     *
     */
    protected function setUp()
    {
        parent::setUp();

    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testGetLeague()
    {
        $repo = $this->createMock();
        $repo->method('registerAnonymous')->willReturn('safak');
        $repo->method('getScores')->willReturn('safak');
        $repo->method('login')->willReturn('safak');

        return $repo;
    }
}
