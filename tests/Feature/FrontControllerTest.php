<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FrontControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * Test Get League Endpoint
     */
    public function testGetLeague()
    {
        $response = $this->get(route('getLeague'));
        $response->assertStatus(200);
        $response->assertSee('Premier League Predictor');
    }
}
