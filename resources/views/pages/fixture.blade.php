@extends('layouts.default')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <td colspan="3">Fixture</td>
                    </tr>
                    </thead>
                    <tbody id="table-body">
                    @if (!empty($weeks))
                        @foreach($weeks as $week)
                            <tr>
                                <td colspan="3">{{$week->name}} Matches</td>
                            </tr>
                            @if (!empty($fixture))
                                @foreach ($fixture[$week->id] as $results)

                                    <tr>
                                        <td><img width="30" height="30"
                                                 src="{{ asset('images/'.$results['home_logo']) }}"/> {{$results['home_team']}}
                                        </td>
                                        <td>{{$results['home_goal']}} - {{$results['away_goal']}}</td>
                                        <td><img width="30" height="30"
                                                 src="{{ asset('images/'.$results['away_logo']) }}"/> {{$results['away_team']}}
                                        </td>
                                    </tr>

                                @endforeach
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>

                <table class="table">
                    <tr>
                        <td>
                            <button onclick="goBack();" class="btn btn-success" id="play-all">Back</button>
                        </td>

                        <td>
                            <button class="btn btn-danger" id="reset">Reset Fixture</button>
                        </td>

                        <td>
                            <a href="{{route('simulation')}}" class="btn btn-info" id="reset">Simulation</a>
                        </td>

                    </tr>
                </table>

            </div>
        </div>
    </div>

@stop
