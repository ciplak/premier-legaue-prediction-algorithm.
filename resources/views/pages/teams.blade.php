@extends('layouts.default')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <td colspan="7">League Table</td>
                    </tr>
                    <tr>
                        <td>Teams</td>
                        <td>PTS</td>
                        <td>P</td>
                        <td>W</td>
                        <td>D</td>
                        <td>L</td>
                        <td>GD</td>
                    </tr>
                    </thead>
                    <tbody id="leauge-table-body">
                    @if (!empty($league))
                        @foreach ($league as $lg)
                            <tr>
                                <td><img width="50" height="50" src="{{ asset('images/'.$lg->logo) }}"/> {{$lg->name}}
                                </td>
                                <td>{{ $lg->points }}</td>
                                <td>{{ $lg->played }}</td>
                                <td>{{ $lg->won }}</td>
                                <td>{{ $lg->draw }}</td>
                                <td>{{ $lg->lose }}</td>
                                <td>{{ $lg->goal_drawn }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>

            <table class="table">
                <tr>
                    <td class="text-center">
                        <a href="{{route('generateFixture')}}" class="btn btn-info">Generate Fixture</a>
                    </td>
                    <td>
                        <button class="btn btn-danger" id="reset">Reset Fixture</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
@stop
