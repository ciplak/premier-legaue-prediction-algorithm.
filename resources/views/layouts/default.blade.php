<!doctype html>
<html>
    <head>
        @include('includes.head')
    </head>
    <body>
        <div class="jumbotron text-center">
            <a href="/">
                <img class="jumbotronwidth" alt=" " src="{{asset('images/logo.png')}}">
            </a>
            <div>
            <h1 class="title-custom">Premier League Predictor</h1>
            </div>

        </div>
        <div id="main" class="row">
            @yield('content')
        </div>
        @include('includes.footer')
    </body>
</html>
