<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 14 May 2018 16:37:38 +0000.
 */

namespace App\Models;

use App\Presenters\PresenterTrait;
use App\Presenters\WeekPresenter;
use McCool\LaravelAutoPresenter\HasPresenter;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Week
 *
 * @property int $id
 * @property string $name
 * @property int $season_id
 *
 * @package App\Models
 */
class Week extends Eloquent implements HasPresenter
{
    use PresenterTrait;

    protected $casts = [
        'season_id' => 'int'
    ];

    protected $fillable = [
        'name',
        'season_id'
    ];

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return $this->present(WeekPresenter::class);
    }
}
