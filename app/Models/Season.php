<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 14 May 2018 16:37:38 +0000.
 */

namespace App\Models;

use App\Presenters\PresenterTrait;
use App\Presenters\SeasonPresenter;
use McCool\LaravelAutoPresenter\HasPresenter;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Season
 *
 * @property int $id
 * @property string $name
 * @property bool $finished
 *
 * @package App\Models
 */
class Season extends Eloquent implements HasPresenter
{
    use PresenterTrait;

    protected $casts = [
        'finished' => 'bool'
    ];

    protected $fillable = [
        'name',
        'finished'
    ];

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return $this->present(SeasonPresenter::class);
    }
}
