<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 14 May 2018 16:37:38 +0000.
 */

namespace App\Models;

use App\Presenters\PresenterTrait;
use App\Presenters\TeamPresenter;
use McCool\LaravelAutoPresenter\HasPresenter;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Team
 *
 * @property int $id
 * @property string $name
 * @property string $logo
 *
 * @package App\Models
 */
class Team extends Eloquent implements HasPresenter
{
    use PresenterTrait;

    protected $fillable = [
        'name',
        'logo'
    ];

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return $this->present(TeamPresenter::class);
    }
}
