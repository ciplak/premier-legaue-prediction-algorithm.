<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 14 May 2018 16:37:38 +0000.
 */

namespace App\Models;

use App\Presenters\PresenterTrait;
use McCool\LaravelAutoPresenter\HasPresenter;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Match
 *
 * @property int $id
 * @property int $week_id
 * @property int $home
 * @property int $away
 *
 * @package App\Models
 */
class Match extends Eloquent implements HasPresenter
{
    use PresenterTrait;

    protected $casts = [
        'week_id' => 'int',
        'home' => 'int',
        'away' => 'int'
    ];

    protected $fillable = [
        'week_id',
        'home',
        'away'
    ];

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return $this->present(MatchPresenter::class);
    }
}
