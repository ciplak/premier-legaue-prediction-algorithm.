<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 14 May 2018 16:37:38 +0000.
 */

namespace App\Models;

use App\Presenters\PresenterTrait;
use App\Presenters\TeamStrengthPresenter;
use McCool\LaravelAutoPresenter\HasPresenter;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TeamStrength
 *
 * @property int $id
 * @property int $team_id
 * @property bool $is_home
 * @property string $strength
 *
 * @package App\Models
 */
class TeamStrength extends Eloquent implements HasPresenter
{
    use PresenterTrait;

    protected $casts = [
        'team_id' => 'int',
        'is_home' => 'bool'
    ];

    protected $fillable = [
        'team_id',
        'is_home',
        'strength'
    ];

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return $this->present(TeamStrengthPresenter::class);
    }
}
