<?php

namespace App\Repositories;

use App\Models\League;
use App\Models\Team;

class TeamRepository
{
    protected $model;
    protected $team;
    public $result = [];

    /**
     * TeamRepository constructor.
     *
     * @param League $model
     * @param Team   $team
     */
    public function __construct(League $model, Team $team)
    {
        $this->model = $model;
        $this->team = $team;
    }

    /**
     * Get All Teams
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->team
            ->leftJoin('league', 'teams.id', '=', 'league.team_id')
            ->orderBy('league.points', 'DESC')->get();
    }

    /**
     * Get All Team Ids
     *
     * @return mixed
     */
    public function getTeams()
    {
        return $this->team->pluck('id');
    }

}
