<?php

namespace App\Repositories;

use App\Models\Team;
use App\Models\Match;
use App\Models\TeamStrength;
use App\Models\Week;

class PlayRepository
{
    protected $team;
    protected $match;
    protected $week;
    protected $teamStrength;
    public $result;

    const WIN_POINTS = 3;
    const LOSE_POINTS = 0;
    const DRAWN_POINTS = 1;
    const COUNTER = 1;

    const WEAK_START = 0;
    const WEAK_FINISH = 2;
    const AVERAGE_START = 2;
    const AVERAGE_FINISH = 3;
    const STRONG_START = 4;
    const STRONG_FINISH = 5;

    const START_VALUE = 0;

    const FIRST_ITEM = 0;
    const SECOND_ITEM = 1;

    const IS_PLAYED = 0;

    /**
     * PlayRepository constructor.
     *
     * @param Team $team
     * @param Match $match
     * @param Week $week
     * @param TeamStrength $teamStrength
     */
    public function __construct(Team $team, Match $match, Week $week, TeamStrength $teamStrength)
    {
        $this->team = $team;
        $this->match = $match;
        $this->week = $week;
        $this->teamStrength = $teamStrength;
    }

    /**
     * Get Team Id.
     *
     * @return mixed
     */
    public function getTeamId()
    {
        return $this->team->pluck('id');
    }

    /**
     * Get Weeks Id.
     *
     * @return mixed
     */
    public function getWeeksId()
    {
        return $this->week->pluck('id');
    }

    /**
     * Get Weeks.
     *
     * @return mixed
     */
    public function getWeeks()
    {
        return $this->week->get();
    }

    /**
     * Create Fixture.
     *
     * @return mixed
     */
    public function createFixture()
    {
        foreach ($this->getWeeksId() as $week) {
            foreach ($this->iterateTeams($this->getTeamId()) as $value) {
                if (self::START_VALUE == $this->checkMatch($week, $value)) {
                    $this->match->create(
                        [
                            'home' => $value[self::FIRST_ITEM],
                            'away' => $value[self::SECOND_ITEM],
                            'week_id' => $week
                        ]
                    );
                }
            }
        }
    }

    /**
     * Iterate Teams.
     *
     * @param  $teams
     * @return array
     */
    private function iterateTeams($teams)
    {
        $collection = collect($teams);
        $matrix = $collection->crossJoin($teams);
        $data = $matrix->reject(
            function ($items) {
                if ($items[self::FIRST_ITEM] == $items[self::SECOND_ITEM]) {
                    return $items;
                }
            }
        )->shuffle();

        return $data->all();
    }

    /**
     * Check Match.
     *
     * @param  $week_id
     * @param  $teams
     * @return mixed
     */
    public function checkMatch($week_id, $teams)
    {
        return $this->match->where('week_id', '=', $week_id)
            ->whereRaw('(home IN(' . implode(',', $teams) . ') OR away IN(' . implode(',', $teams) . '))')
            ->count();
    }


    /**
     * Get Fixture.
     *
     * @return mixed
     */
    public function getFixture()
    {
        return $this->match->select(
            'matches.id',
            'matches.played',
            'matches.week_id',
            'matches.home_goal',
            'matches.away_goal',
            'week_id',
            'home.name as home_team',
            'home.logo as home_logo',
            'away.logo as away_logo',
            'away.name as away_team'
        )
            ->join('weeks', 'weeks.id', '=', 'matches.week_id')
            ->join('teams as home', 'home.id', '=', 'matches.home')
            ->join('teams as away', 'away.id', '=', 'matches.away')
            ->orderBy('week_id', 'ASC')
            ->get();
    }

    /**
     * Get Fixture By Week Id.
     *
     * @param  $week_id
     * @return mixed
     */
    public function getFixtureByWeekId($week_id)
    {
        return $this->match->select(
            'matches.id',
            'matches.played',
            'matches.week_id',
            'matches.home_goal',
            'matches.away_goal',
            'week_id',
            'weeks.name',
            'home.logo as home_logo',
            'away.logo as away_logo',
            'home.name as home_team',
            'away.name as away_team'
        )
            ->join('weeks', 'weeks.id', '=', 'matches.week_id')
            ->join('teams as home', 'home.id', '=', 'matches.home')
            ->join('teams as away', 'away.id', '=', 'matches.away')
            ->where('matches.week_id', '=', $week_id)
            ->orderBy('matches.id', 'ASC')
            ->get();
    }

    /**
     * Get Team Strenght.
     *
     * @param  $team_id
     * @param  $is_home
     * @return mixed
     */
    public function getTeamStrenght($team_id, $is_home)
    {
        return $this->teamStrength->where([['team_id', '=', $team_id], ['is_home', '=', $is_home]])->get();
    }

    /**
     * Create Strenght.
     *
     * @param  $team_id
     * @param  $is_home
     * @return int
     */
    public function createStrenght($team_id, $is_home)
    {

        foreach ($this->getTeamStrenght($team_id, $is_home) as $value) {
            switch ($value->strength) {
                case 'strong':
                    $this->result = rand(self::STRONG_START, self::STRONG_FINISH);
                    break;
                case 'average':
                    $this->result = rand(self::AVERAGE_START, self::AVERAGE_FINISH);
                    break;
                case 'weak' :
                    $this->result = rand(self::WEAK_START, self::WEAK_FINISH);
                    break;
            }

            return $this->result;
        }
    }

    /**
     * Get Matches From Week.
     *
     * @param  $week
     * @return mixed
     */
    public function getMatchesFromWeek($week)
    {
        return $this->match
            ->where([['week_id', '=', $week], ['played', '=', self::IS_PLAYED]])
            ->get();
    }

    /**
     * Get All Matches.
     *
     * @param  int $played
     * @return mixed
     */
    public function getAllMatches($played = self::IS_PLAYED)
    {
        return $this->match->where('played', '=', $played)->get();
    }

    /**
     * Calculate Score
     *
     * @param  $homeScore
     * @param  $awayScore
     * @param  $home
     * @param  $away
     * @return mixed
     */
    public function calculateScore($homeScore, $awayScore, $home, $away)
    {
        if ($homeScore > $awayScore) {
            $this->homeBiggerThanAwayScore($homeScore, $awayScore, $home, $away);
        } elseif ($awayScore > $homeScore) {
            $this->awayBiggerThanHomeScore($homeScore, $awayScore, $home, $away);
        } else {
            $this->awayHomeDrawn($home, $away);
        }

        $this->homeAwayCalculateScore($home, $away);
    }


    /**
     * Truncate Matches.
     *
     * @return mixed
     */
    public function truncateMatches()
    {
        $this->match->truncate();
    }

    /**
     * Get All Strenght.
     *
     * @return mixed
     */
    public function getAllStrenght()
    {
        return $this->teamStrength
            ->select('team_strengths.id', 'teams.name', 'teams.logo', 'team_strengths.is_home', 'team_strengths.strength')
            ->join('teams', 'teams.id', '=', 'team_strengths.team_id')
            ->orderBy('teams.id')
            ->get();
    }


    /**
     * Update Match.
     *
     * @param  $id
     * @param  $column
     * @param  $value
     * @return mixed
     */
    public function updateMatch($id, $column, $value)
    {
        $match = $this->match->find($id);
        $match->{$column} = $value;
        $match->played = 1;
        $match->save();

        return $match;
    }

    /**
     * Home Bigger Than Away Score.
     *
     * @param $homeScore
     * @param $awayScore
     * @param $home
     * @param $away
     */
    protected function homeBiggerThanAwayScore($homeScore, $awayScore, $home, $away): void
    {
        $home->won += self::COUNTER;
        $home->points += self::WIN_POINTS;
        $home->goal_drawn += ($homeScore - $awayScore);
        $away->lose += self::COUNTER;
        $away->goal_drawn += ($awayScore - $homeScore);
    }

    /**
     * Away Bigger Than Home Score
     *
     * @param $homeScore
     * @param $awayScore
     * @param $home
     * @param $away
     */
    protected function awayBiggerThanHomeScore($homeScore, $awayScore, $home, $away): void
    {
        $away->won += self::COUNTER;
        $away->points += self::WIN_POINTS;
        $away->goal_drawn += ($awayScore - $homeScore);
        $home->lose += self::COUNTER;
        $home->goal_drawn += ($homeScore - $awayScore);
    }

    /**
     * Away Home Drawn
     *
     * @param $home
     * @param $away
     */
    protected function awayHomeDrawn($home, $away): void
    {
        $home->draw += self::COUNTER;
        $away->draw += self::COUNTER;
        $home->points += self::COUNTER;
        $away->points += self::COUNTER;
    }

    /**
     * Home Away CalculateScore.
     *
     * @param $home
     * @param $away
     */
    protected function homeAwayCalculateScore($home, $away): void
    {
        $home->played += self::COUNTER;
        $away->played += self::COUNTER;
        $home->save();
        $away->save();
    }
}
