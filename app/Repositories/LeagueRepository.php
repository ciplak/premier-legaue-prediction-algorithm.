<?php

namespace App\Repositories;

use App\Models\League;
use App\Models\Team;

class LeagueRepository
{
    protected $model;
    protected $team;
    public $result = [];

    const DEFAULT_VAL = 0;

    /**
     * LeagueRepository constructor.
     *
     * @param League $model
     * @param Team   $team
     */
    public function __construct(League $model, Team $team)
    {
        $this->model = $model;
        $this->team = $team;

    }

    /**
     * Get All Assets.
     *
     * @return mixed
     */
    public function getAll()
    {
        return $this->team
            ->leftJoin('league', 'teams.id', '=', 'league.team_id')
            ->orderBy('league.points', 'DESC')->get();
    }

    /**
     * Get Teams.
     *
     * @return mixed
     */
    public function getTeams()
    {
        return $this->team->pluck('id');
    }

    /**
     * Create League.
     *
     * @return mixed
     */
    public function createLeague()
    {
        $result = $this->model->get();
        if ($result->isEmpty()) {
            foreach ($this->getTeams() as $value) {
                $data = [
                    'team_id' => $value,
                    'points' => self::DEFAULT_VAL,
                    'played' => self::DEFAULT_VAL,
                    'won' => self::DEFAULT_VAL,
                    'lose' => self::DEFAULT_VAL,
                    'draw' => self::DEFAULT_VAL,
                    'goal_drawn' => self::DEFAULT_VAL
                ];
                $this->model->create($data);
            }
        }
    }

    /**
     * Update League.
     *
     * @param  array $data
     * @param  $team_id
     * @return mixed
     */
    public function updateLeauge($data = array(), $team_id)
    {
        return $this->model
            ->where('team_id', '=', $team_id)
            ->update($data);
    }

    /**
     * Get League By Team Id
     *
     * @param  $team_id
     * @return mixed
     */
    public function getLeaugeByTeamId($team_id)
    {
        return $this->model
            ->where('team_id', $team_id)
            ->first();
    }

    /**
     * Truncate Leauge
     *
     * @return mixed
     */
    public function truncateLeauge()
    {
        $this->model->truncate();
    }
}
