<?php

namespace App\Http\Controllers;

use App\Repositories\LeagueRepository;
use App\Repositories\PlayRepository;

class FrontController extends Controller
{
    protected $league;
    protected $teams;
    protected $weeks;
    protected $leagueRepository;
    protected $playRepository;
    protected $fixture;
    protected $result = [];
    protected $predictions = [];

    const HOME = 1;
    const AWAY = 0;

    const WON = 3;
    const COUNTER = 1;
    /**
     * HomeController constructor.
     *
     * @param LeagueRepository $leagueRepository
     * @param PlayRepository $playRepository
     */
    public function __construct(LeagueRepository $leagueRepository, PlayRepository $playRepository)
    {
        $this->leagueRepository = $leagueRepository;
        $this->playRepository = $playRepository;
        $this->weeks = $this->playRepository->getWeeks();
        $this->fixture = $this->playRepository->getFixture();

        $this->leagueRepository->createLeague();
    }

    /**
     * Get League
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLeague()
    {
        $this->playRepository->createFixture();
        $strength = $this->playRepository->getAllStrenght();

        return view(
            'pages/teams',
            [
                'league' => $this->league,
                'weeks' => $this->weeks,
                'strength' => $strength,
                'types' => ['weak', 'average', 'strong']
            ]);
    }

    /**
     *  Generate Fixture.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function generateFixture()
    {
        $collection = collect($this->fixture);
        $grouped = $collection->groupBy('week_id');

        return view(
            'pages/fixture',
            [
                'league' => $this->league,
                'matches' => $grouped->toArray(),
                'fixture' => $grouped->toArray(),
                'weeks' => $this->weeks,
            ]);
    }

    /**
     * Refresh Fixture.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshFixture()
    {
        $collection = collect($this->fixture);
        $grouped = $collection->groupBy('week_id');
        return response()->json(['weeks' => $this->weeks, 'items' => $grouped->toArray()]);
    }

    /**
     *
     */
    public function play()
    {
        $matches = $this->playRepository->getAllMatches();
        $this->playGame($matches);
    }

    /**
     * Refresh League.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshLeauge()
    {
        $this->league = $this->leagueRepository->getAll();
        return response()->json($this->league);
    }


    /**
     * Play Weekly.
     *
     * @param $week
     * @return \Illuminate\Http\JsonResponse
     */
    public function playWeekly($week)
    {
        $matches = $this->playRepository->getMatchesFromWeek($week);
        $this->playGame($matches);
        $result = $this->playRepository->getFixtureByWeekId($week);

        return response()->json(['matches' => $result]);
    }


    /**
     * Truncate ORM(reset)
     */
    public function reset()
    {
        $this->playRepository->truncateMatches();
        $this->leagueRepository->truncateLeauge();
        $this->playRepository->createFixture();
    }

    /**
     * Play Game.
     *
     * @param $matches
     */
    private function playGame($matches)
    {
        foreach ($matches as $match) {
            $this->startPlayGame($match);
        }

    }

    /**
     * Next Matches.
     *
     * @param $week
     * @return \Illuminate\Http\JsonResponse
     */
    public function nextMatches($week)
    {
        $matches = $this->playRepository->getFixtureByWeekId($week);

        return response()->json(['matches' => $matches]);
    }

    /**
     * Predictions
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function predictions()
    {
        $finished = $this->leagueRepository->getAll();
        $this->collectionPredictions($finished);
        $matches = $this->playRepository->getAllMatches();
        $this->combinePredictions($matches);
        $collection = collect($this->predictions);
        $multiplied = $collection->map(function ($item) {
            return round((($item['points'] / $this->sumPoints()) * 100), 2);
        });

        $combine = $multiplied->all();

        //reset keys after combine
        $values = $collection->values();

        $chart = [];
        foreach ($values->all() as $key => $val) {
            array_push($chart, [$val['name'], $combine[$val['team_id']]]);
        }

        return response()->json(array('items' => $chart));
    }

    /**
     * Combine Predictions
     *
     * @param $matches
     */
    private function combinePredictions($matches)
    {
        foreach ($matches as $match) {
            $homeScore = $this->playRepository->createStrenght($match->home, self::HOME);
            $awayScore = $this->playRepository->createStrenght($match->away, self::AWAY);

            $points = $this->calculateScoreForChart($homeScore, $awayScore);
            if (isset($points['away'])) {
                foreach ($points['away'] as $key => $value) {
                    $this->predictions[$match->away][$key] += $points['away'][$key];
                }
            }
            if (isset($points['home'])) {
                foreach ($points['home'] as $key => $value) {
                    $this->predictions[$match->home][$key] += $points['home'][$key];
                }
            }
        }
    }

    /**
     * Collection Predictions.
     *
     * @param $data
     */
    private function collectionPredictions($data)
    {
        $collection = collect($data);
        $collection->each(function ($item) {
            $this->predictions[$item->team_id]['points'] = $item->points;
            $this->predictions[$item->team_id]['name'] = $item->name;
            $this->predictions[$item->team_id]['team_id'] = $item->team_id;
        });
    }

    /**
     * Calculate Score For Chart.
     *
     * @param $homeScore
     * @param $awayScore
     * @return array
     */
    public function calculateScoreForChart($homeScore, $awayScore)
    {
        $points = [];
        if ($homeScore > $awayScore) {
            $points['home']['points'] = self::WON;
        } elseif ($awayScore > $homeScore) {
            $points['away']['points'] = self::WON;
        } else {
            $points['home']['points'] = self::COUNTER;
            $points['away']['points'] = self::COUNTER;
        }

        return $points;
    }

    /**
     * Sum Points.
     *
     * @return float|int
     */
    private function sumPoints()
    {
        return array_sum(array_map(function ($item) {
            return $item['points'];
        }, $this->predictions));
    }

    /**
     * Update Match.
     *
     * @param $id
     * @param $column
     * @param $value
     */
    public function updateMatch($id, $column, $value)
    {
        $this->playRepository->updateMatch($id, $column, $value);
        $this->leagueRepository->truncateLeauge();
        $this->leagueRepository->createLeague();
        $matches = $this->playRepository->getAllMatches(1);

        foreach ($matches as $match) {
            $home = $this->leagueRepository->getLeaugeByTeamId($match->home);
            $away = $this->leagueRepository->getLeaugeByTeamId($match->away);
            $this->playRepository->calculateScore($match->home_goal, $match->away_goal, $home, $away);
        }
    }

    /**
     * Simulation.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function simulation()
    {
        $this->weeks = $this->playRepository->getWeeks();
        $this->league = $this->leagueRepository->getAll();
        $this->playRepository->createFixture();
        $strength = $this->playRepository->getAllStrenght();
        $this->fixture = $this->playRepository->getFixture();
        $collection = collect($this->fixture);
        $grouped = $collection->groupBy('week_id');

        return view(
            'pages/simulation',
            [
                'league' => $this->league,
                'matches' => $grouped->toArray(),
                'fixture' => $grouped->toArray(),
                'weeks' => $this->weeks,
                'strength' => $strength,
                'types' => ['weak', 'average', 'strong']
            ]);
    }

    /**
     * Start Play Game
     *
     * @param $match
     */
    private function startPlayGame($match): void
    {
        $homeScore = $this->playRepository->createStrenght($match->home, self::HOME);
        $awayScore = $this->playRepository->createStrenght($match->away, self::AWAY);
        $home = $this->leagueRepository->getLeaugeByTeamId($match->home);
        $away = $this->leagueRepository->getLeaugeByTeamId($match->away);
        $this->playRepository->calculateScore($homeScore, $awayScore, $home, $away);
        $match->home_goal = $homeScore;
        $match->away_goal = $awayScore;
        $match->played = self::COUNTER;
        $match->save();
    }
}
