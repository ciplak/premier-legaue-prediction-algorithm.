<?php

namespace App\Presenters;


use App\Models\Team;
use App\Repositories\TeamRepository;
use Carbon\Carbon;
use McCool\LaravelAutoPresenter\BasePresenter;

/**
 * Class TeamRepository
 */
class TeamPresenter extends BasePresenter
{
    /**
     * @var TeamRepository
     */
    private $team;

    /**
     * TeamPresenter constructor.
     *
     * @param TeamRepository $team
     * @param Team           $resource
     */
    public function __construct(TeamRepository $team, Team $resource)
    {
        $this->team = $team;
        $this->wrappedObject = $resource;
    }


    /**
     * Points.
     *
     * @return int|mixed
     */
    public function points()
    {
        return $this->wrappedObject->points ?? 0;
    }


    /**
     * Played.
     *
     * @return int|mixed
     */
    public function played()
    {
        return $this->wrappedObject->played ?? 0;
    }


    /**
     * Won.
     *
     * @return int|mixed
     */
    public function won()
    {
        return $this->wrappedObject->won ?? 0;
    }

    /**
     * Draw
     *
     * @return int|mixed
     */
    public function draw()
    {
        return $this->wrappedObject->draw ?? 0;
    }

    /**
     * Lose.
     *
     * @return int|mixed
     */
    public function lose()
    {
        return $this->wrappedObject->lose ?? 0;
    }


    /**
     * Goal Drawn.
     *
     * @return int|mixed
     */
    public function goal_drawn()
    {
        return $this->wrappedObject->goal_drawn ?? 0;
    }
}
