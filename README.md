# Anonymous - Premier League Simulation


## Run following commands

```command
composer install
```

```command
yarn
```

```command

bower install
```


## Setup your .env file and create database written in your .env file
```command

cp .env.example .env

```


```command

php artisan key:generate
```

```command

php artisan migrate --seed

```

```command

./vendor/bin/phpcbf app

```


```command

./vendor/bin/phpcs app

```

```command

php artisan serve

```

